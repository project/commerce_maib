# Commerce MAIB

The Commerce MAIB (Moldova Agroindbank Payment Gateway) Drupal Module
is used to easily integrate the MAIB Payment into your project. Based on
the maib/maibapi library to connect and process the requests with the Bank
server and log the requests responses.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/commerce_maib).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/commerce_maib).


## Requirements

- [Commerce module](https://www.drupal.org/project/commerce)
- [Maib Library](https://packagist.org/packages/maib/maibapi)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

After installation, add a new payment Gateway on
`admin/commerce/config/payment-gateways`:

1. Select MAIB (Off-site redirect) Plugin.
2. Give name, display name.
3. Select the mode (Default is Test).
4. Send an email with your IP and Callback URLs from
   `"Return and cancel URLs to be provided to bank"` tecom@maib.md.
   It is necessary to have access to the bank server.
5. Set the paths to certificate key, certificate and password.
   For tests use Test Private Key, Test Certificate Pass and Test Certificate-
   they are indicated near thfields.
6. Select the Transaction type.
    1. Capture (capture payment immediately after customer's approval)
    2. Authorize (requires manual or automated capture after checkout)
7. Check the Log debug info checkbox and set the path you want to save the logs.
   When you are done testing, the bank will be required the Logs, before setting
   Live Mode.
8. Set up the conditions you want.
9. Set the Status to Enabled
10. Save the configuration form.


## Usage

Try to pay with the new Payment Gateway.

In the test mode you need to use only the test card:

  Card number: 5102180060101124
  Expiration MM/YY: 06/28
  CVV: 760

After successful payment, you will be redirected to the callback URL.

To change the mode to Live, need to send the logs to the bank.
1. The Bank will send you your own certificate.
2. Need to generate the key and certificate based on this.

    INSTRUCTIONS TO EXTRACT KEYS FROM PFX FILE

    Use openssl to extract keys from PFX file and password provided by bank
    Public key chain:
      openssl pkcs12 -in certname.pfx -nokeys -out cert.pem
    The private key with password:
      openssl pkcs12 -in certname.pfx -nocerts -out key.pem
    Or optionally without a password:
      openssl pkcs12 -in certname.pfx -nocerts -out key.pem -nodes

    CentOS note, curl+nss requires rsa + des3 for private key:
      openssl rsa -des3 -in key.pem -out key-des3.pem

3. Upload these to the server folder.
4. Change the paths to a new certificate and key.


## Maintainers

- Adrian Golub - [adrian.golub](https://www.drupal.org/u/adriangolub)
- Andrei Vesterli - [andrei.vesterli](https://www.drupal.org/u/andreivesterli)
- dscutaru - [dscutaru](https://www.drupal.org/u/dscutaru)
- maib-ecomm - [maib-ecomm](https://www.drupal.org/u/maib-ecomm)
