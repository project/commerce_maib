<?php

namespace Drupal\commerce_maib\PluginForm\OffsiteRedirect;

use Drupal\commerce_maib\Exception\MAIBException;
use Drupal\commerce_maib\MAIBGateway;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PaymentOffsiteForm for Maib.
 *
 * @package Drupal\commerce_maib\PluginForm\OffsiteRedirect
 *
 * @return array $form
 *   The base payment offsite form for maib.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The commerce maib logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a new PaymentOffsiteForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager object.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory object.
   */
  final public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->logger = $logger_factory->get('commerce_maib');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $payment->getPaymentGateway();
    /** @var \Drupal\commerce_maib\Plugin\Commerce\PaymentGateway\OffsiteRedirect $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();

    $form['#capture'] = isset($configuration['intent']) && $configuration['intent'] == 'capture';

    $redirect_url = $payment_gateway_plugin->getRedirectUrl();
    $redirect_method = PaymentOffsiteForm::REDIRECT_POST;

    $capture = !empty($form['#capture']);
    $currency = $payment->getAmount()->getCurrencyCode();
    /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currencyObj */
    $currencyObj = $this->entityTypeManager->getStorage('commerce_currency')
      ->load($currency);
    $amount = $payment->getAmount()->getNumber();

    $client_ip_addr = $payment->getOrder()->getIpAddress();
    $description = (string) $this->t('Order #@id', ['@id' => $payment->getOrderId()]);
    $language = $this->languageManager
      ->getCurrentLanguage()
      ->getId();

    $transaction_id = NULL;
    try {
      $client = $payment_gateway_plugin->getClient();

      if ($capture) {
        $response = $client->registerSmsTransaction($amount, $currencyObj->getNumericCode(), $client_ip_addr, $description, $language);
      }
      else {
        $response = $client->registerDmsAuthorization($amount, $currencyObj->getNumericCode(), $client_ip_addr, $description, $language);
      }

      if (isset($response['error'])) {
        throw new MAIBException(sprintf('MAIB error: %s', $response['error']));
      }
      elseif (!isset($response[MAIBGateway::MAIB_TRANSACTION_ID])) {
        throw new MAIBException('MAIB error: Missing TRANSACTION_ID');
      }
      else {
        $transaction_id = $response[MAIBGateway::MAIB_TRANSACTION_ID];
      }

      $pending_payment = $payment_gateway_plugin->storePendingPayment($payment->getOrder(), $transaction_id);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      throw new MAIBException(sprintf('MAIB error: %s', $e->getMessage()));
    }

    $this->logger->notice($this->t(
      'Got transaction id @trans_id for order @order and payment @payment',
      [
        '@trans_id' => $transaction_id,
        '@order' => $payment->getOrderId(),
        '@payment' => $pending_payment->id(),
      ])
    );

    $data = [
      MAIBGateway::MAIB_TRANS_ID => $transaction_id,
      'language' => $language,
    ];

    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, $data, $redirect_method);

    return $form;
  }

}
